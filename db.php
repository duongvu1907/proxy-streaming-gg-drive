<?php
$config = require_once('config/service.php');
$servername = $config["database"]["hostname"];
$username = $config["database"]["username"];
$password = $config["database"]["password"];
$db = $config["database"]["dbname"];
//GLOBAL $conn;
$conn = new mysqli($servername, $username, $password,$db);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

include "model/func.php";