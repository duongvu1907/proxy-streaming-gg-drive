<?php

$video_id = isset($_GET["data"])?$_GET["data"]:"";
if ($video_id=='') {
    exit("Error 403");
} 
$arr = explode(".",str_replace("\"","",$video_id));
if (count($arr)!=2) {
    exit("Error 403");
}
$hash = $arr[0];
$id = $arr[1];
if ($hash!=md5($id)) {
    exit("Error 403");
}

include "db.php";
if (!checkIDExists($id,"streams","id")) {
    exit("Error 404");
}
include_once 'func.php';

include_once 'php_fast_cache.php';

include_once 'packer.php';

$query = "SELECT * FROM streams Where id=".$id;
$video = ($GLOBALS["conn"]->query($query))->fetch_assoc()["view_id"];

$cache = phpFastCache::get($id[1]);
if ($cache == NULL) {
    $sources = Drive($id[1]);
    phpFastCache::set($id[1], $sources, '7200');
} else $sources = $cache;
echo $sources;