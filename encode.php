
<?php

error_reporting(0);
require "middleware.php";
include 'db.php';

include_once 'func.php';

include_once 'php_fast_cache.php';

include_once 'packer.php';

$link = (isset($_GET['link'])) ? $_GET['link'] : 'https://drive.google.com/file/d/0BwHxX3yoJoeuYzZlNm1jYVhwWWs/view?usp=sharing';
preg_match('/https?:\/\/(?:www\.)?(?:drive|docs)\.google\.com\/(?:file\/d\/|open\?id=)?([a-z0-9A-Z_-]+)(?:\/.+)?/is', $link, $id);
$title = isset($_GET['title'])?$_GET['title']:time()."";

$drive_id = $_GET['folder'];



$cache = phpFastCache::get($id[1]);
if ($cache == NULL) {
    $sources = Drive($id[1]);
    phpFastCache::set($id[1], $sources, '7200');
} else $sources = $cache;

if ($drive_id!="") {
  $folder =  addFolder($drive_id);
}
// echo $sources;
if (!checkIDExists($id[1],"streams","view_id")) {
    addStream($id[1],$title,$folder["dirve_id"],"",$sources);
}
$xx = getByFilter(1,"folders","id");
$stream_id = getIdByKey($id[1]);
$url = "source.php?data=".md5($stream_id).".".$stream_id;
$allData = [
    "folder"=>$folder,
    "stream"=>[
        "view_id"=>$id[1],
        "filename"=>$title,
        "source"=>$sources,
        "id" =>  $stream_id,
        "url"=>$url
    ]
];
// echo json_encode($xx);
echo json_encode($allData);
