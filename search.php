<?php
include "middleware.php";
if($_SERVER["REQUEST_METHOD"]=="GET"){
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search Link</title>
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="assets/images/logo.png" alt="logo" width="50px;"></a>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo03">

                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="alone.php">Alone <span class=" sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="search.php">Search <span class="sr-only">(current)</span></a>
                    </li>
                </ul>

            </div>
        </nav>
        <div class="row align-items-center justify-content-center">
            <div class="col-md-6">
                <form action="search.php" method="post">
                    <div class="form-group">
                        <label for="">Keyword</label>
                        <input type="text" id="key" require="" class="form-control">
                    </div>
                    <button class="btn btn-success" type="button" onclick="asubmit()">submit</button>
                </form>
                
            </div>
        </div>
        <hr>
        <br>
        <div id="result2">

                </div>
    </div>
    <script>
        function asubmit()
        {
            var key = $("#key").val().trim()
            $.ajax({
                type: "post",
                url: "search.php",
                data: {key},
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    var asd = "";
                    for (let i = 0; i < response.length; i++) {
                       asd+=`
                        <tr>
                            <td>${response[i].id}</td>
                            <td>${response[i].title}</td>
                            <td>${response[i].file_name}</td>
                            <td>${response[i].shortlink}</td>
                            <td>${response[i].created_at}</td>
                        </tr>
                       `
                    }
                    var dom = `
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>title</th>
                            <th>filename</th>
                            <th>short link</th>
                            <th>created_at</th>
                        </tr>
                        <tr>
                            <td>${asd}</td>
                        </tr>
                    </table>
                    `
                    $("#result2").html(dom);
                }
            });
        }
    </script>
</body>

</html>
<?php }else{
 include "db.php";
 $key = $_POST["key"];
 $query = "SELECT * From streams Where title like '%".$key."%' or file_name like '%".$key."%'";
 $data = $GLOBALS["conn"]->query($query);
 $arr = [];
 if ($data->num_rows>0) {
    while($row=$data->fetch_assoc()){
        $row["shortlink"] = md5($row["id"]).".".$row["id"];
        array_push($arr,$row);
    }    
 }
 echo json_encode($arr);

}


?>