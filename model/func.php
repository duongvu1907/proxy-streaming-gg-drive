<?php

function checkIDExists($id,$table,$column=""){
    if ($column=="") {
        $column="id";
    }
    $query = "SELECT * FROM ".$table." Where ".$column."='".$id."'";
    $data = $GLOBALS["conn"]->query($query);
    // return $query;
    if($data->num_rows >0){
        return true;
    };
    return false;
};
function getIdByKey($view_id){
    $query = "SELECT * FROM streams Where view_id='".$view_id."' limit 1";
    $data = $GLOBALS["conn"]->query($query);
    return $data->fetch_assoc()["id"];
}
function addFolder($id){
    $share_url = generateLinkShare($id);
    if (!checkIDExists($id,"folders","drive_id")) {
       $query = "INSERT INTO folders(drive_id,share_url) VALUES('".$id."','".$share_url."')";
        $GLOBALS["conn"]->query($query);
    }
    return [
        "drive_id"=>$id,
        "share_url"=>$share_url
    ];
}
function addStream($view_id,$file_name,$folder_id,$title="",$encode_url){
    $raw_link = generateLinkView($view_id);
    $value="'".$file_name."','".$raw_link."','".$folder_id."','".$encode_url."','".$view_id."','".$title."'";
    $query = "INSERT INTO streams(file_name, raw_url, folder_id, encode_url, view_id, title ) VALUES(".$value.")";
    if ($GLOBALS["conn"]->query($query)) {
        return true;
    }
    exit("Error save streams");
}
function generateLinkShare($id)
{
    return "https://drive.google.com/drive/folders/".$id."?usp=sharing";
};
function generateLinkView($id)
{
    return "https://drive.google.com/file/d/".$id."/view";
};
function getByFilter($id,$table,$column){
    $query = "SELECT * FROM ".$table." Where ".$column."=".$id."";
    $data = $GLOBALS["conn"]->query($query);
    $xx = [];
    if ($data->num_rows>0) {
        while($row = $data->fetch_assoc()) {
        array_push($xx,$row);
          }
    }
    return $xx;
}
function saveTitle($id,$title){
    $query = "update streams set title='".$title."' where id=".$id;
    $GLOBALS["conn"]->query($query);
}