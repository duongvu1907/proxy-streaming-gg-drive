function get(){
    $('#result').html('<img src="assets/images/loading.gif" class="mx-auto d-block mt-2"/>');
    $('#fa-loading').html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
    
    var link = $('#link').val();
    $.ajax({
        url:'gdrive_list/action.php',
        type:'post',
        dataType:'text',
        data: {link : link},
        error: function (result) {
            $('#result').html('Error!');
            $('#action').removeAttr('disabled');
            $('#fa-loading').html('<i class="fa fa-arrow-circle-right fa-fw"></i>');
        },
        success: function (result) {
            $('#result').html(result);
            $('#action').removeAttr('disabled');
            $('#fa-loading').html('<i class="fa fa-arrow-circle-right fa-fw"></i>');
        }
    });
}
function Export(btn){
    var uri = $(btn).attr('data-url').trim();
    var name = $(btn).attr('data-file').trim();
    var folder = $(btn).attr('data-folder').trim();
    var link = $("#proxy");
    var filename = $("#filename")
    link.val(uri);
    filename.val(name);
    $("#folder").val(folder);
    // link.attr('value',uri);
}
function proxyServer(){
    $('#result2').html('<img src="assets/images/loading.gif" class="mx-auto d-block mt-2"/>');
    $('#fa-loading2').html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
    
    var link = $('#proxy').val();
    var folder = $('#folder').val();
    var title = $('#filename').val()==''?(new Date).getTime()+'':$('#filename').val();
    $.ajax({
        url:'encode.php',
        type:'get',
        dataType:'text',
        data: {link,title,folder},
        error: function (result) {
            $('#result2').html('Error!');
            $('#fa-loading2').html('<i class="fa fa-arrow-circle-right fa-fw"></i>');
        },
        success: function (result) {
            $result = JSON.parse(result);
            var dom = `
            <p><b>Drive Folder:</b> <span>${$result.folder.drive_id}</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="${$result.folder.share_url}" target="_blank" class="btn btn-danger">see</a></p>
            <form action="save.php" method="post">
            <button class='btn btn-success' onclick='saveStream()' type="button">Save</button>  <span class="notify"></span>
            <div class="row">
                <input type="hidden" id="id" value="${$result.stream.id}" />   
                <div class="col-md-6">
                    <label>Title</label>
                    <input type="text" id="title" class="form-control">
                </div>
                <div class="col-md-6">
                    <label>Video ID</label>
                    <input type="text" id="view_id" value="${$result.stream.view_id}" readonly="" class="form-control">
                </div>
                <div class="col-md-6">
                    <label class="text-danger">Short link</label>
                    <input type="text"  value="${$result.stream.url}" onclick="this.select()" readonly="" class="form-control">
                </div>
                <div class="col-md-6">
                    <label for="">filename</label>
                    <input type="text" id="filename" value="${$result.stream.filename}" readonly="" class="form-control">
                </div>
                
                <div class="col-md-6">
                    <textarea name="source" class="form-control" readonly="">${$result.stream.source}</textarea>
                </div>
            </div>
        </form>`
            $("#result2").html(dom);
            $('#fa-loading2').html('<i class="fa fa-arrow-circle-right fa-fw"></i>');
        }
    });
}
function saveStream()
{
    var id= $("#id").val();
    var title = $("#title").val();
    $.ajax({
        type: "post",
        url: "save.php",
        data: {id,title},
        dataType: "json",
        error: function (result) {
            console.log(result.responseText)
            $(".notify").html(result.responseText);
        },
        success: function (response) {
            $(".notify").html(response.responseText);
            console.log(response.responseText);
        }
    });
}