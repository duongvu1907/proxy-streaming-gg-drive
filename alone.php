<?php
include "middleware.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    include 'db.php';

    include_once 'func.php';

    include_once 'php_fast_cache.php';

    include_once 'packer.php';
    $link = (isset($_GET['link'])) ? $_GET['link'] : 'https://drive.google.com/file/d/0BwHxX3yoJoeuYzZlNm1jYVhwWWs/view?usp=sharing';
    preg_match('/https?:\/\/(?:www\.)?(?:drive|docs)\.google\.com\/(?:file\/d\/|open\?id=)?([a-z0-9A-Z_-]+)(?:\/.+)?/is', $link, $id);
    $cache = phpFastCache::get($id[1]);
    if ($cache == NULL) {
        $sources = Drive($id[1]);
        phpFastCache::set($id[1], $sources, '7200');
    } else $sources = $cache;

    if (!checkIDExists($id[1], "streams", "view_id")) {
        addStream($id[1], "untitled", 1, "", $sources);
    }
    $stream_id = getIdByKey($id[1]);
    $url = "source.php?data=" . md5($stream_id) . "." . $stream_id;
    $allData = [
        "view_id" => $id[1],
        "filename" => "untitled",
        "source" => $sources,
        "id" =>  $stream_id,
        "url"=>$url
    ];
    // echo json_encode($xx);
    echo json_encode($allData);
} else {
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Generate Stream Link</title>
        <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" />
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>

    <body>
        <div class="container">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="assets/images/logo.png" alt="logo" width="50px;"></a>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">

                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="alone.php">Alone <span class=" sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="search.php">Search <span class="sr-only">(current)</span></a>
                        </li>
                    </ul>

                </div>
            </nav>
            <div class="row align-items-center justify-content-center">
                <div class="col-md-6">
                    <form action="alone.php" method="post">
                        <div class="form-group">
                            <label for="">Link GG Video</label>
                            <input type="text" id="gvideo" require="" class="form-control">
                        </div>
                        <button class="btn btn-success" type="button" onclick="asubmit()">submit</button>
                    </form>
                    <div id="result2"></div>
                </div>
            </div>
        </div>
        <script>
            function asubmit() {
                var $gvideo = $("#gvideo").val().trim();
                $.ajax({
                    type: "post",
                    url: "alone.php",
                    data: {
                        url: $gvideo
                    },
                    dataType: "json ",
                    success: function($result) {
                        // $result = JSON.parse(response);
                        var dom = `<br><hr><br>
            
            <form action="save.php" method="post">
            <button class='btn btn-success' onclick='saveStream()' type="button">Save</button>  <span class="notify"></span>
            <div class="row">
                <input type="hidden" id="id" value="${$result.id}" />   
                <div class="col-md-6">
                    <label>Title</label>
                    <input type="text" id="title" class="form-control">
                </div>
                <div class="col-md-6">
                    <label>Video ID</label>
                    <input type="text" id="view_id" value="${$result.view_id}" readonly="" class="form-control">
                </div>
                <div class="col-md-6">
                    <label class="text-danger">Short link</label>
                    <input type="text"  value="${$result.url}" onclick="this.select()" readonly="" class="form-control">
                </div>
                <div class="col-md-6">
                    <label for="">filename</label>
                    <input type="text" id="filename" value="${$result.filename}" readonly="" class="form-control">
                </div>
                
                <div class="col-md-6">
                    <textarea name="source" class="form-control" readonly="">${$result.source}</textarea>
                </div>
            </div>
        </form>`
                        $("#result2").html(dom);
                    }
                });
            }
        </script>
        <script src="assets/javascripts/script.js"></script>
    </body>

    </html>
<?php } ?>