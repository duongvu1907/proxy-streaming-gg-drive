<?php 
error_reporting(0);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		
		if (!empty($_POST['link'])) {
		   
		   include_once 'curl.php';
		   
		   $curl = new cURL();
		   
		   $link = $_POST['link'];

		   preg_match('/.*\/folders\/(.*)(\?.*|\/)/', $link, $matchID);

		   $getFileList = $curl->get('https://www.googleapis.com/drive/v2/files?q=%27' . $matchID[1] . '%27%20in%20parents&maxResults=9999&orderBy=title&key=AIzaSyBXV3qGJ2rwDaxvUmAzaVpZMmn1t6PyU0E');
		 	
		 	$deJson = json_decode($getFileList);

		    $i = 0;

			$result = '';

		 	foreach ($deJson->items as $key => $value) {
			   	$i++;
				$result .=	'<div class="input-group mb-3">
							  <input type="text" class="form-control" value="'.$value->title.'" onclick="this.select()">
                              <input type="text" class="form-control" value="'.$value->alternateLink.'" onclick="this.select()">
                              <div class="input-group-prepend">
							    <button class="input-group-text" onclick="Export(this)" data-folder="'.$matchID[1].'" data-file="'.$value->title.'" data-url="'.$value->alternateLink.'">Export</button>
							  </div>
							</div>';
		 	}

		 	$result = str_replace('?usp=drivesdk', '', $result);

		    echo $result;

		} else echo '<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Empty link!</div>';

	} else echo '<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Error Request!</div>';

?>