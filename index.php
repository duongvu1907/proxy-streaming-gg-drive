<?php
require "middleware.php";
if ($_SERVER["REQUEST_METHOD"]=="POST") {
    $_SESSION["user"]="";
    session_unset($_SESSION["user"]);
    header("Location: login.php");
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Google Drive File List in Folder - API Codes</title>
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
    <div class="container-fluid">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="assets/images/logo.png" alt="logo" width="50px;"></a>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo03">

                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="alone.php">Alone <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="search.php">Search <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0" action="" method="post">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
                </form>
            </div>
        </nav>
        <div class="row jutify-content-center">
            <div class="col-lg-6">
                <div class="container">
                    <div class="jumbotron mt-3">
                        <h1 class="display-6">Google Drive File List in Folder</h1>
                        <p class="lead">Help you to get all videos link in Google Drive folder easily.</p>
                        <p class="lead">Ex: https://drive.google.com/drive/folders/1rjIPNyOcyJUpcNFB_0a6rsH4s_lRlhJr?usp=sharing</p>
                    </div>

                    <form method="post" accept-charset="utf-8">
                        <div class="input-group">

                            <input type="text" name="link" id="link" class="form-control" placeholder="Enter Google Drive folder link here..." onclick="this.select()">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="button" onclick="get()"><span id="fa-loading"><i class="fa fa-arrow-circle-right fa-fw"></i></span> Button</button>
                            </div>
                        </div>
                    </form>

                    <div class="text-center mt-3">
                        <div id="result"></div>
                    </div>


                </div>
            </div>
            <div class="col-lg-6">
                <div class="jumbotron mt-3">
                    <h1 class="display-6">Generate Link Proxy Streaming</h1>
                    <p class="lead">Help you encode URL to protect your real URL and support multiple subtitles. You can use url or iframe after encoding into your website easily and quickly.</p>
                    <p class="lead">Ex: https://your-domain.com/proxy.php?data=&key=</p>
                </div>

                <form method="post" accept-charset="utf-8">
                    <div class="input-group">
                        <input type="hidden" name="filename" id="filename">
                        <input type="hidden" name="folder" id="folder">
                        <input type="text" name="proxy" id="proxy" class="form-control" placeholder="Enter Google Drive Video link here..." onclick="this.select()">
                        <div class="input-group-append">
                            <button class="btn btn-success" type="button" onclick="proxyServer()"><span id="fa-loading2"><i class="fa fa-arrow-circle-right fa-fw"></i></span> Button</button>
                        </div>
                    </div>
                </form>

                <div class="text-center mt-3">
                    <div id="result2"></div>
                </div>
            </div>
        </div>
        <hr>
        <footer class="footer">
            <p class="text-center">Copyright 2020 © Toocin.com . All rights reserved.</p>
        </footer>
    </div>
    <script src="assets/javascripts/script.js"></script>
</body>

</html>