<?php
include "db.php";

if ($_SERVER["REQUEST_METHOD"]=="POST") {
    $title = $_POST["title"];
    $id = (int)$_POST["id"];
    if ($title=="") {
        exit("title is required");
    }
    if (checkIDExists($id,"streams","id")) {
        saveTitle($id,$title);
        echo "Successful";
    }else{
        exit("no exists");
    }
}